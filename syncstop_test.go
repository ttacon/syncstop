package syncstop

import (
	"testing"
	"time"
)

func TestSyncStop(t *testing.T) {
	s := NewSyncStop()

	go func(s SyncStop) {
		select {
		case <-s.Stopped():
			s.Finish()
		case <-time.After(time.Second):
			t.Error("stop timed out")
		}
	}(s)

	s.Stop()

	s.Finished()
}

func TestSyncStop_FinishedWithTimeout(t *testing.T) {
	s := NewSyncStop()

	go func(s SyncStop) {
		select {
		case <-s.Stopped():
			s.Finish()
		case <-time.After(time.Second):
			t.Error("stop timed out")
		}
	}(s)

	s.Stop()

	s.FinishedWithTimeout(time.Second)
}

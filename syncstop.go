package syncstop

import "time"

type SyncStop interface {
	Stop()
	Stopped() <-chan struct{}

	Finish()
	Finished()
	FinishedWithTimeout(dur time.Duration)
}

type syncStop struct {
	stop   chan struct{}
	finish chan struct{}
}

func NewSyncStop() SyncStop {
	return &syncStop{
		stop:   make(chan struct{}),
		finish: make(chan struct{}),
	}
}

func (s *syncStop) Stop() {
	s.stop <- struct{}{}
}

func (s *syncStop) Stopped() <-chan struct{} {
	return s.stop
}

func (s *syncStop) Finish() {
	s.finish <- struct{}{}
}

func (s *syncStop) Finished() {
	<-s.finish
}

func (s *syncStop) FinishedWithTimeout(dur time.Duration) {
	select {
	case <-s.finish:
	case <-time.After(dur):
	}
}
